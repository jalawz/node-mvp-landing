const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const request = require('request');
const async = require('async');
const expressHbs = require('express-handlebars');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');

const app = express();

// mongodb://<dbuser>:<dbpassword>@ds119449.mlab.com:19449/newsletter

app.engine('.hbs', expressHbs({ defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', 'hbs');

app.use(express.static(`${__dirname}/public`));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: 'paulinholindo',
  store: new MongoStore({url: 'mongodb://root:penha269@ds119449.mlab.com:19449/newsletter'})
}));
app.use(flash());


app.route('/')
  .get((req, res, next) => {
    res.render('main/home', {message: req.flash('success')});
  })
  .post((req, res, next) => {
    request({
      url: 'https://us13.api.mailchimp.com/3.0/lists/090836d08d/members',
      method: 'POST',
      headers: {
        'Authorization': 'randomUser e26af0f32daf6f7cd9c5bded370eb993',
        'Content-Type': 'application/json'
      },
      json: {
        'email_address': req.body.email,
        'status': 'subscribed'
      }
    }, (err, response, body) => {
      if (err) {
        console.log(err);
      } else {
        console.log('Successfully sent');
        req.flash('success', 'You have submitted your email');
        res.redirect('/');
      }
    })
  })


app.listen(3000, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('Running on port 3000');
  }
});